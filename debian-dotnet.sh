#!/bin/bash
# prepare
apt-get update
apt-get install -y wget curl
## dotnet sdk
### add microsoft to apt
wget https://packages.microsoft.com/config/debian/12/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
dpkg -i --force-confnew packages-microsoft-prod.deb
rm packages-microsoft-prod.deb
apt-get update
### install dotnet sdk's
apt-get install -y dotnet-sdk-8.0
apt-get install -y dotnet-sdk-7.0
apt-get install -y dotnet-sdk-6.0
dotnet --info
### configure
export DOTNET_CLI_TELEMETRY_OPTOUT=1
### install tools
dotnet tool install --global dotnet-reportgenerator-globaltool --version 5.2.0
### set path
export PATH="${PATH:+${PATH}:}$HOME/.dotnet/tools/reportgenerator"
## docker
### add Docker's official GPG key
curl -fsSL https://get.docker.com -o get-docker.sh
chmod +x get-docker.sh
sh get-docker.sh
### check version
docker -v
# clean up
apt-get remove -y wget curl
